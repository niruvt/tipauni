--------------------------------------------------------------------------
Package:      tipauni
Author:       निरंजन
Version:      v0.3  (01 October, 2021)
Description:  For producing Unicode characters with TIPA commands.
Repository:   https://gitlab.com/niruvt/tipauni
Bug tracker:  https://gitlab.com/niruvt/tipauni/-/issues
License:      GPL v3.0+, GFDL v1.3+
--------------------------------------------------------------------------
